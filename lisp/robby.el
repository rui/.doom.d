
(defvar robby--scripts-location (expand-file-name "~/Sync/code/robby/robby/xontrib"))
(defvar robby--py-scripts-location (expand-file-name "~/Sync/code/robby/robby/robby"))

;; utils
(defun robby--open-firefox (url)
  (interactive)
  (cond
   ((string-equal system-type "windows-nt") ; Windows
    (shell-command (concat "firefox file://" url)))
   ((string-equal system-type "gnu/linux")
    (shell-command (concat "firefox file://" url)))
   ((string-equal system-type "darwin") ; Mac
    (shell-command (concat "open -a Firefox.app " url)))))


(defun robby-notes-move ()
  "Move the Obsidian notes"
  (interactive)
  (compile (concat robby--scripts-location "/notes.xsh move")))

;; Humble routines

(defun robby-humble-build ()
  "Build the blog"
  (interactive)
  (compile (concat robby--scripts-location "/humble.xsh build")))

(defun robby--get-paired-nb-name (filename)
  "Return the paired notebook name for a specific FILENAME."
  (concat (file-name-sans-extension filename) ".ipynb"))

(defun robby-humble-open-notebook ()
  "Open the associated notebook in a separate buffer."
  (interactive)
  (setq notebook-name (robby--get-paired-nb-name buffer-file-name))
  (message notebook-name)
  (shell-command (concat "euporie-notebook " notebook-name))
)

(defun robby-humble-render-paired-notebook ()
  "Build the notebook paired with this buffer."
  (interactive)
  (compile (concat robby--scripts-location
                   "/humble.xsh quarto -f "
                   (shell-quote-argument (robby--get-paired-nb-name buffer-file-name)))))

(defun robby-humble-hugo-dev-server ()
  "Start a Hugo dev server"
  (interactive)
  (compile "cd ~/Sync/code/sites/blog-source-obsidian ; hugo -D server --disableFastRender --cleanDestinationDir  --ignoreCache --gc")
  (robby--open-firefox "http://localhost:1313/"))

;; Admin functions

;;;; Backup functions

(defun robby-backup-kopia-wasabi ()
  "Backup to Wasabi using Kopia."
  (interactive)
  (compile (concat
            robby--py-scripts-location
            "/backup.py kopia connect-wasabi ;"
            robby--py-scripts-location
            "/backup.py kopia backup")))

(defun robby-backup-kopia-local ()
  "Backup locally using Kopia."
  (interactive)
  (compile (concat
            robby--py-scripts-location
            "/backup.py kopia connect-local ;"
            robby--py-scripts-location
            "/backup.py kopia backup")))

(map! :leader
      (:prefix-map ("r" . "robby")
       (:prefix ("n" . "notes")
        :desc "Move notes" "m" #'robby-notes-move
        :desc "Another entry" "s" #'robby-notes-move)))

(map! :leader
      (:prefix-map ("r" . "robby")
       (:prefix ("w" . "workflow")
        :desc "Backup to Wasabi with Kopia" "w" #'robby-backup-kopia-wasabi
        :desc "Backup to locally with Kopia" "l" #'robby-backup-kopia-local)))

(map! :leader
      (:prefix-map ("r" . "robby")
       (:prefix ("h" . "humble")
        :desc "Build the blog" "b" #'robby-humble-build
        :desc "Render paired notebook" "r" #'robby-humble-render-paired-notebook
        :desc "Start Hugo dev server" "h" #'robby-humble-hugo-dev-server)))

(provide 'robby)

;;; robby.el ends here
